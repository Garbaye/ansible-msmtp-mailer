#!/usr/bin/env bash

FROMFEDORA='39'
PACKAGE='msmtp'

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
cd ${ABSDIR}

type -P docker >/dev/null && DOCKER=$(which docker)
type -P podman >/dev/null && DOCKER=$(which podman)

get_src_rpm_url() {
    local __package=${1}
    local __resultvar=${2}
    if ! local __jsonline=$(curl -s https://src.fedoraproject.org/_dg/bodhi_updates/rpms/${1} | grep -e "fc${FROMFEDORA}\","); then
	echo "ERROR : No ${1} release available for Fedora ${FROMFEDORA}"
	echo "Please check https://src.fedoraproject.org/rpms/${PACKAGE} to choose the right release."
	exit 1
    fi
    local __version=$(echo "${__jsonline}" | sed -e "s/^\s*\"stable\": \"${1}-\(.*\)\.fc${FROMFEDORA}\",\s*$/\1/")
    for repo in releases updates; do
	local __url=https://dl.fedoraproject.org/pub/fedora/linux/${repo}/${FROMFEDORA}/Everything/source/tree/Packages/${__package::1}/${__package}-${__version}.fc${FROMFEDORA}.src.rpm
	if curl -sSLfI ${__url} 1>/dev/null 2>/dev/null; then
	    eval $__resultvar="'${__url}'"
	    return 0
	fi
    done
    exit 1
}

get_src_rpm_url ${PACKAGE} PACKAGE_SRC_RPM_URL
get_src_rpm_url gettext GETTEXT_SRC_RPM_URL
get_src_rpm_url automake AUTOMAKE_SRC_RPM_URL
get_src_rpm_url autoconf AUTOCONF_SRC_RPM_URL

ARCH=$(${DOCKER} run --rm docker.io/library/rockylinux:8 uname -i)

mkdir /tmp/dockerrpmbuild-$$

${DOCKER} run -i --rm -v /tmp/dockerrpmbuild-$$:/out:Z --privileged docker.io/library/rockylinux:8 <<EOF
dnf install -qy epel-release
dnf install -qy mock
useradd mockbuilder
usermod -a -G mock mockbuilder
runuser -l mockbuilder -c 'mock -v --localrepo=/tmp/ --chain -r rocky+epel-8-${ARCH} ${AUTOCONF_SRC_RPM_URL} ${AUTOMAKE_SRC_RPM_URL} ${GETTEXT_SRC_RPM_URL} ${PACKAGE_SRC_RPM_URL}'
mv -f /tmp/results/*/${PACKAGE}-*/*.rpm /out/
EOF

[[ $? == 0 ]] && mv -i /tmp/dockerrpmbuild-$$/*.rpm ${ABSDIR}/files/ && rm -rf /tmp/dockerrpmbuild-$$
